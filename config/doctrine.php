<?php
return [
    "doctrine" => [
        'isDevMode' => true,
        "params" => [
            'driver' => 'pdo_sqlite',
            'path' => dirname(__DIR__) . '/data/marcus.db',
//            "host" => "db.example.com",
//            "username" => "dbuser",
//            "password" => "secret",
//            "dbname" => "marcus_db",
        ],
        'paths' => [
            dirname(__DIR__) . '/src/App/Entity/'
        ]
    ]
];