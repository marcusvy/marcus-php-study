<?php

return [
    'container' => [
        /**
         * Services
         * associative array that maps a key to a service instance.
         */
        'services' => [],

        /**
         * Invokables
         *  An associative array that maps a key to a constructor-less service; i.e., for services that do not require arguments to the constructor. The key and service name usually are the same; if they are not, the key is treated as an alias.
         */
        'invokables' => [],

        /**
         * Factories
         * Associative array that map a key to a factory name, or any callable.
         */
        'factories' => [
            \Doctrine\ORM\EntityManager::class => \Marcus\Factory\EntityManagerFactory::class
        ],

        /**
         * Abstract Factories
         * A list of abstract factories classes. An abstract factory is a factory that can potentially create any object, based on some criterias.
         */
        'abstract_factories' => [],

        /**
         * Delegators
         * An associative array that maps service keys to lists of delegator factory keys, see the delegators documentation for more details.
         * @link https://docs.zendframework.com/zend-servicemanager/delegators/
         */
        'delegators' => [],

        /**
         * Aliases
         * Associative array that map a key to a service key (or another alias).
         */
        'aliases' => [],

        /**
         * Initializers
         * A list of callable or initializers that are run whenever a service has been created.
         */
        'initializers' => [],

        /**
         * Lazy Services
         * Configuration for the lazy service proxy manager, and a class map of service:class pairs that will act as lazy services; see the lazy services documentation for more details.
         */
        'lazy_services' => [],

        /**
         * Shared
         * Associative array that maps a service name to a boolean, in order to indicate to the service manager whether or not it should cache services it creates via get method, independent of the shared_by_default setting.
         */
        'shared' => [],

        /**
         * Shared By Default
         * Boolean that indicates whether services created through the get method
         */
        'shared_by_default' => true,
    ]
];