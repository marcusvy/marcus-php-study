<?php

namespace Marcus\Factory;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;
use Interop\Container\ContainerInterface;
use Marcus\Kernel;
use Zend\ServiceManager\Factory\FactoryInterface;

class EntityManagerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return EntityManager
     * @throws \Doctrine\ORM\ORMException
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get(Kernel::CONFIG)[Kernel::DOCTRINE];
        $doctrineConfig = Setup::createAnnotationMetadataConfiguration(
            $config['paths'],
            $config['isDevMode'],
            null,
            null,
            false
        );
        return EntityManager::create($config['params'], $doctrineConfig);
    }
}
