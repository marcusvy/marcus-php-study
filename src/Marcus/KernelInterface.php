<?php

namespace Marcus;

interface KernelInterface
{
    /**
     * Run Kernel
     * @return void
     */
    public function run();
}
