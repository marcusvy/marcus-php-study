<?php

namespace Marcus;

use Doctrine\ORM\Tools\Console\ConsoleRunner;
use Doctrine\ORM\EntityManager;

class KernelDoctrineCli implements KernelInterface
{
    use KernelTrait;

    public function run()
    {
        $this->bootstrap();
        $em = $this->container->get(EntityManager::class);
        return ConsoleRunner::createHelperSet($em);
    }
}
