<?php

namespace Marcus;

use Zend\ServiceManager\ServiceManager;
use Zend\Config\Factory as ConfigFactory;

trait KernelTrait
{
    /**
     * @var array
     */
    public $config = [];

    /**
     * @var ServiceManager
     */
    public $container;

    /**
     * Create Container Manager
     */
    public function createServiceManager()
    {
        $configContainerFile = realpath(dirname(__DIR__) . '/../config/container.php');
        $config = ConfigFactory::fromFile($configContainerFile);
        $this->container = new ServiceManager($config['container']);
    }

    /**
     * Load configurations
     */
    public function createConfiguration()
    {
        $this->container->setFactory(Kernel::CONFIG, function () {
            $configFiles = glob(dirname(__DIR__) . '/../config/*.php');
            return ConfigFactory::fromFiles($configFiles);
        });
        $this->config = $this->container->get(Kernel::CONFIG);
    }

    /**
     * Bootstrap Application
     */
    public function bootstrap()
    {
        $this->createServiceManager();
        $this->createConfiguration();
    }
}
