<?php

namespace Marcus;

use Symfony\Component\Console\Application;

class Kernel implements KernelInterface
{
    use KernelTrait;

    const CONFIG = 'config';
    const DOCTRINE = 'doctrine';

    /**
     * @var Application
     */
    public $application;

    /**
     * Create Console Application
     */
    public function createApplication()
    {
        $this->application = new Application();
    }

    /**
     * Run console
     */
    public function runConsole()
    {
        $this->bootstrap();
        $this->createApplication();

        foreach ($this->config['commands'] as $command) {
            $this->application->add(new $command());
        }
        $this->application->run();
    }

    /**
     * Run Kernel
     */
    public function run()
    {
        $this->bootstrap();
        $this->createApplication();

        $handler = $this->config['handler']['main'];
        call_user_func(new $handler(), $this->container);
    }
}
