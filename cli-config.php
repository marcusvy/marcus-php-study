<?php
use Marcus\KernelDoctrineCli;

require_once 'vendor/autoload.php';
chdir(__DIR__);

$kernel = new KernelDoctrineCli();
return $kernel->run();